#include "section.h"
#include <QTime>
#include <QObject>

Section::Section()
{
    //init members
    duration = new QTime(0,1,0,0);
    setName("");
    setTitle("");
    setCentralTitle("");
    init();
}

Section::Section(const QString &name){

    duration = new QTime(0,1,0,0);
    setName(name);
    setTitle(QObject::tr("Descriptive title"));
    setCentralTitle(QObject::tr("Central title during first section's minute, then timer"));
    init();
}

Section::Section(const QString &name,const QTime &time, const QString &title){

    duration = new QTime(time.hour(),time.minute(),time.second(),time.msec());
    setName(name);
    setTitle(title);
    setCentralTitle("");
    init();
}

void Section::setName(const QString &name_){
    name.clear();
    name.append(name_);
}

void Section::setTitle(const QString &title_){
    title.clear();
    title.append(title_);    
}

void Section::setDuration( const QTime &duration_){
    //if valid checked
    duration->setHMS(duration_.hour(),duration_.minute(),duration_.second(),duration_.msec());
}

void Section::setCentralTitle(const QString &str){
    centralTitle.clear();
    centralTitle.append(str);
}

void Section::init(){
    titleColor = Qt::white;
    titleFont = QFont();

    centralTitleColor = Qt::white;
    centralTitleFont = QFont();

    timerColor = Qt::white;
    timerFont = QFont();
    timerFont.setPointSize(24);

    runningTimeColor = Qt::darkGreen;
    runningTimeFont = QFont();

}
