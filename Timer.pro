#-------------------------------------------------
#
# Project created by QtCreator 2011-11-06T13:20:04
#
#-------------------------------------------------

QT       += core gui

TARGET = Kipot
TEMPLATE = app


SOURCES += main.cpp \
    mainwindow.cpp \
    section.cpp \
    showwidget.cpp \
    timeniceformat.cpp \
    listwidget.cpp

HEADERS  += \
    mainwindow.h \
    section.h \
    showwidget.h \
    timeniceformat.h \
    listwidget.h

FORMS    += \
    mainwindow.ui \
    showwidget.ui

ICON = images/Timer.icns

RESOURCES += \
    images.qrc

TRANSLATIONS = timer_es.ts

OTHER_FILES += \
    notes.txt \
    Revisions.txt \
    bugs.txt







































