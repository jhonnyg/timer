#include "listwidget.h"
#include <QDropEvent>
#include <QDebug>
ListWidget::ListWidget(QWidget *parent) :
    QListWidget(parent)
{
    dropping=false;
}

void ListWidget::dragEnterEvent(QDragEnterEvent *event)
{
    qDebug() <<"QlistWidget dragEvent crow"<<this->currentRow();
    QListWidget::dragEnterEvent(event);
    event->acceptProposedAction();
    init = this->currentRow();
    dropping = true;
}

void ListWidget::dropEvent(QDropEvent *event)
{    
    int from = this->currentRow();

//    qDebug() <<"dropevent "<<this->currentRow();
    //    qDebug() << "count "<<this->count();

//    if(this->count()==1){
//        QListWidget::dropEvent(event);
//    }
//    else{

//        init = this->currentRow();
        //        dropping = true;

        QListWidget::dropEvent(event);
        int to = this->currentRow();
        //        emit drop(init);
        //        dropping=false;
        qDebug() <<"Drop event from "<< from << " to "<< to;
        qDebug();
        emit drop(from, to);
//    }
}

int ListWidget::getInit() const
{
    return init;
}

void ListWidget::setInit(int value)
{
    init = value;
}

void ListWidget::setDropping(bool value)
{
    dropping = value;
}
