#ifndef SHOWWIDGET_H
#define SHOWWIDGET_H

#include <QWidget>
#include <QDateTime>
#include <QtGui>
#include "section.h"
#include "ui_showwidget.h"
//#include "timeniceformat.h"

//class QList<Section>;
class QTimer;
class QString;

//This class manages the view showPanel, where the time, title,
// time left to finish section... are shown.
// It contains a list of sections with their characteristics
// And can edit them
class ShowWidget : public QWidget, public Ui::ShowWidget
{
    Q_OBJECT

public:
    explicit ShowWidget(QWidget *parent = 0);
    Section getSectionFromList(int row);
    Section *getSectionInList(int row);
    void sliderWasMovedbyUser(const QTime &time);

    QTime totalTime();//returns total current time of show

    void setRunningTimeColor(QColor color){runningTimeColor = color;}
    QColor getRunninTimeColor(){return runningTimeColor;}
    void setRunningTimeFont(QFont font){runningTimeFont = QFont(font);}
    QFont getRunningTimeFont(){return runningTimeFont;}

    void setTimerColor(QColor color){timerColor = color;}
    QColor getTimerColor(){return timerColor;}
    void setTimerFont(QFont font){timerFont = QFont(font);}
    QFont getTimerFont(){return timerFont;}
    void setTimer2Color(QColor color){timerColor2 = color;}
    QColor getTimer2Color(){return timerColor2;}

signals:
    void timeout();

public slots:
    void play(bool);
    void addSection(const QString &name, int row);
    void removeSection(int pos);
    void duplicateSection(int pos); // duplicate section at position pos
    void drop(int from, int to);
    void removeAll();
    int countSections(){return sectionList.size();}
    bool handleNextSection();//is it private??
    void handlePresentation();// too?
    void currentRowInViewChanged(int);
    void textChanged(const QString &str, int); // was private slot
    void increaseSpeedFactor();
    void decreaseSpeedFactor();
    void setSpeedFactorToOne();
    void setSectionId(int id){sectionId=id;}

    bool writeFile(const QString &file);
    bool readFile(const QString &file);

    void setSectionTitle(int id, const QString &str);
    void setSectionCentralTitle(int id, const QString &str);
    void setSectionTitleColor(int id, QColor col);
    void setSectionCentralTitleColor(int id, QColor col);

protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *);

private slots:

signals:
    void stop();
    void presentationTimeChanged(QTime);
    void updateShowWidgetView();

private:

    //methods
    int timeInSeconds(const QTime &time);
    int computeFontPointSize(const QString &str,QFont font, int w, int h );    


    //
    QDateTime finishTime;
    QTimer *updateTimer; //To update screen every second

    QTime presentationTime; //count total running time of presentation
    QTime sectionTime; // time of current section
    QTime timeToFinishSection;

    QList<Section> sectionList;
    QColor runningTimeColor;
    QFont runningTimeFont;
    QColor timerColor;
    QColor timerColor2;
    QFont timerFont;

    QString leftText;
    QString centralText;
    QString rightText;
    // or Section currentSection; // it contains all info
    int sectionId;
    double speedFactor;
    bool isPlaying;

    enum {MagicNumber = 0xA0B0C0D0, version = 100};   

};

#endif // SHOWWIDGET_H
