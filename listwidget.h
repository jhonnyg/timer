#ifndef LISTWIDGET_H
#define LISTWIDGET_H

#include <QListWidget>

class ListWidget : public QListWidget
{
    Q_OBJECT
public:
    explicit ListWidget(QWidget *parent = 0);
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
    bool isDropping(){return dropping;}
    int getInit() const ;
    void setInit(int value);    
    void setDropping(bool value);

signals:
    void drop(int from, int to);

public slots:

private:
    bool dropping;
    int init;
};

#endif // LISTWIDGET_H
