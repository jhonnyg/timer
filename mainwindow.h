#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "showwidget.h"
#include <QMainWindow>
#include <mainwindow.h>
#include "ui_mainwindow.h"



class MainWindow : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void newFile();
    void newTimer();
    void open();
    bool save();
    bool saveAs();
    void showWidgetmodified();
    void openRecentFile();
    void updateRecentFileActions();
    void clearMenu();
    void fullscreen();    
    void selectFont();
    void selectColor();
    void addSection();
    void removeSection();
    void duplicateSection();
    void copySection();
    void drop(int from, int to);
    void play(bool);
    void play();
    void stop();
    void currentItemChanged(int row);
    void currentItemChanged( QListWidgetItem * current, QListWidgetItem * previous );
    void help();

    void timeChanged( const QTime &time);
    void sectionNameChanged();

    void sliderMovedbyUser(int);
    void updateSlider(QTime);
    void updateView();
    void setSliderMaxValue();
    void updateLabel(QTime time);
    void configChanged();

    void titleChanged();
    void titleColorChanged();
    void titleFontChanged();

    void centralTitleChanged();
    void centralTitleColorChanged();
    void centralTitleFontChanged();

    void runningTimeColorChanged();
    void runningFontChanged();
    void timerColorChanged();
    void timerColor2Changed();
    void timerFontChanged();
    void about();
    void checkNewVersion();

    void test();
    void updateWidgetFocus(QWidget *old, QWidget *neww);    


private:
    void createActions();

    bool okToContinue();
    void readSettings();
    void writeSettings();
    bool loadFile(const QString &fileName);
    bool saveFile(const QString &fileName);
    void setCurrentFile(const QString &fileName);    
    QString strippedName(const QString &fullFileName);
    bool isAddingSection();
    void update();



    QString currentFile;
    QStringList recentFiles;

    enum {MaxRecentFiles = 5};
    QAction *recentFileActions[MaxRecentFiles];

    QPushButton *plusButton;
    QPushButton *minusButton;
    QAction *actionClear_Menu;
    QAction *actionCopy_Section;

    QWidget *oldWidget;
    QWidget *newWidget;

    QPalette myPalette;

    QPropertyAnimation *animation;
    QStateMachine *machine;
    QState *state1;
    QState *state2;
    QSignalTransition *transition1;
    QSignalTransition *transition2;
    QSequentialAnimationGroup *group1;
};

#endif // MAINWINDOW_H
