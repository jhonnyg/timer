#include <QtGui> //never forget this, needed even for timer
#include <QWidget>
#include "showwidget.h"
#include <iostream>
#include <QString>
#include <QTextCursor>
#include "section.h"
#include "timeniceformat.h"

class MainWindow;

ShowWidget::ShowWidget(QWidget *parent) :
    QWidget(parent)
{
    setupUi(this);
    runningTimeColor = Qt::darkGreen;
    runningTimeFont = QFont();
    runningTimeFont.setPointSize(36);
    timerColor = Qt::darkMagenta;//QColor(255,0,0,125);
    timerColor2 = Qt::red;//QColor(25,0,0,125);
    timerFont = QFont();
    timerFont.setPointSize(48);

    //    resize(qApp->desktop()->width(),qApp->desktop()->height());

    sectionId = 0;
    speedFactor = 1.0;
    isPlaying = false;
    sectionList = QList<Section>();
    addSection(tr("New Section 1"),0);
    getSectionFromList(0).setDuration( QTime(0,1,0,0));
    //Times
    presentationTime = QTime(0,0,0,0);
    sectionTime =  QTime(0,1,0,0);
    timeToFinishSection = QTime(0,1,0,0);

    // Timers
    updateTimer = new QTimer(this);
    connect(updateTimer, SIGNAL(timeout()), this, SLOT(handlePresentation()));

    //    setAutoFillBackground(true);
    //    setPalette(QPalette(Qt::black));

}

void ShowWidget::play(bool playIsChecked){
    if(playIsChecked){
        //        sectionTimer->start(-timeToFinishSection.secsTo(QTime(0,0,0,0))*1000*speedFactor);
        updateTimer->start(1000*speedFactor);
    }else{
        updateTimer->stop();
        //        sectionTimer->stop();
    }
    isPlaying = playIsChecked;
    update();
}

void ShowWidget::resizeEvent(QResizeEvent *event){
    //    QSize size(4,3);
    //    qDebug() <<size;
    //    QSize old = event->oldSize();
    //    size.scale(old,Qt::KeepAspectRatio);
    //    qDebug() <<size;
    //    QResizeEvent *event2 = new QResizeEvent(size,old);
    //    QWidget::resizeEvent(event2);
    QWidget::resizeEvent(event);
}

void ShowWidget::paintEvent(QPaintEvent *event){

    qreal ratio = 4.5/3.0;

    QPainter painter(this);
    painter.setRenderHint(QPainter::HighQualityAntialiasing);

    qreal w = width();
    qreal h = height();//*4/3.0;
    qreal side = qMin(w,h);


    if(! parentWidget()->parentWidget()->isFullScreen())
        painter.fillRect(0,0,w,h,QColor(Qt::gray).lighter(120));

    painter.translate(width()/2.0,height()/2.0);
    painter.scale(side/200.0,side/200.0);

    painter.fillRect(-100*ratio,-100,100*ratio*2.0,200,Qt::SolidPattern);

    if (sectionId >sectionList.size()-1 || sectionId<0)
        sectionId=0;
    Section temp = sectionList.at(sectionId);

    //******************************
    //---------- RUNNING TIME    ***
    //******************************
    QString str = presentationTime.toString(timeSmartFormat(presentationTime));
    QColor color = runningTimeColor;
    painter.setPen(color);
    painter.setFont(runningTimeFont);

    QRectF r(w/8.0,h/4.0,50,50);
    //    r = QRectF(0,0,w/2.0,h/2.0);
    //    r = QRectF(-bs/2.0*w,-100,bs/2.0*w,100);
    r = QRectF(-100*ratio+5,-100,100*ratio,100);

    painter.drawText(r,Qt::AlignVCenter,str);

    //******************************
    //--------    TITLE-----------**
    //******************************
    str = temp.getTitle();
    painter.setPen(temp.getTitleColor());
    painter.setFont(temp.getTitleFont());
    //    r = QRectF(w/2.0,0,w/2.0 ,h/2.0);
    //    r = QRectF(5,-95,bs*w/2.0-10 ,95);
    r = QRectF(5,-100,100*ratio,95);
    //    boundingR = painter.boundingRect(r,Qt::AlignVCenter|Qt::TextWordWrap,str);
    painter.drawText(r,Qt::AlignVCenter|Qt::TextWordWrap, str);

    //******************************
    //           CENTRAL PART      * Or central park, pufff
    //******************************
    if(timeInSeconds(temp.getDuration())-timeInSeconds(timeToFinishSection)<=1 ){
        str = temp.getCentralTitle();
        color = temp.getCentralTitleColor();
        painter.setPen(color);
        painter.setFont(temp.getCentralTitleFont());
    }
    else{
        str = timeToFinishSection.toString(timeSmartFormat(timeToFinishSection));
        painter.setFont(timerFont);
        if(timeToFinishSection <= QTime(0,5,0,0) || timeToFinishSection.minute()%5==0){
            painter.setPen(timerColor2);
        }
        else{
            painter.setPen(timerColor);
        }
    }

    r = QRectF(-100*ratio,0,200*ratio,100);
    painter.drawText(r,Qt::AlignCenter|Qt::TextWordWrap, str);

    //    painter.drawLine(-100,0,100,0);
    //    painter.drawLine(0,100,0,-100);
}

int ShowWidget::computeFontPointSize(const QString &str, QFont font, int w, int h){
    int initQFontPointSize = 80;
    QFontMetrics fm(font);
    int width = fm.width(str);
    int i=0;
    if(width > w){
        while(width+50 > w && i <100){
            font.setPointSize(initQFontPointSize-=5);
            QFontMetrics fm(font);
            width = fm.width(str);
            i++;
        }
    }else{
        while(width+50 < w && i <100){
            font.setPointSize(initQFontPointSize+=5);
            QFontMetrics fm(font);
            width = fm.width(str);
            i++;
        }
    }
    //    qDebug() <<"initQFontPointSize " <<initQFontPointSize;
    return initQFontPointSize;
}

// check this function passing the value of the pointer??
void ShowWidget::addSection(const QString &name, int row){
    sectionList.insert(row,Section(name));
}

void ShowWidget::removeSection(int pos){
    if(!sectionList.isEmpty()){
        sectionList.removeAt(pos);
    }
}

void ShowWidget::duplicateSection(int pos){
    sectionList.insert(pos+1,sectionList.at(pos));
    emit updateShowWidgetView();
}

void ShowWidget::drop(int from, int to)
{
    qDebug() << "ShowWidget *******************************                             drop"<<from << to;
    //    sectionList.
    int t = sectionList.size()-1;
    if(from==to || from==(to+1)){}
    else{
        sectionList.move(from, to);
    }
    //    sectionId=final;
    //    sectionList.removeAt(init);
    emit updateShowWidgetView();
}

//remove all but first, used when a new file empty file is created
void ShowWidget::removeAll(){
    sectionList.insert(0,Section(tr("New Section 1")));
    int temp = sectionList.count();
    for(int i = temp-1; i > 0 ; i--){
        sectionList.removeAt(i);
        temp = sectionList.count();
    }
}

Section ShowWidget::getSectionFromList(int row){
    Section temp;
    if(row<0)
        row=0;
    if(row < sectionList.size()){
        temp = sectionList.at(row);
        //        return sectionList[row];
    }
    return temp;
}

Section *ShowWidget::getSectionInList(int row){
    if(row < sectionList.size())
        return &sectionList[row];
    return 0;
}

void ShowWidget::textChanged(const QString &str, int row){
    if(!sectionList.isEmpty())
        if(0<=row && row<sectionList.size())
            sectionList[row].setName(str);
}

void ShowWidget::setSectionTitle(int id, const QString &str){
    sectionList[id].setTitle(str);
    update();
}

void ShowWidget::setSectionCentralTitle(int id, const QString &str){
    sectionList[id].setCentralTitle(str);
    update();
}

void ShowWidget::setSectionTitleColor(int id, QColor col){
    sectionList[id].setTitleColor(col);
    update();
}

void ShowWidget::setSectionCentralTitleColor(int id, QColor col){
    sectionList[id].setCentralTitleColor(col);
    update();
}

bool ShowWidget::writeFile(const QString &fileName){
    QFile file(fileName);
    if(!file.open(QIODevice::WriteOnly)){
        QMessageBox::warning(this,tr("Timer"),tr("Cannot write file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream out(&file);
    out.setVersion(QDataStream::Qt_4_8);

    out << quint32(MagicNumber);
    out << quint32(version);

    QApplication::setOverrideCursor(Qt::WaitCursor);

    Section temp;
    int size = sectionList.size();
    out << quint16(size);
    out << runningTimeColor <<runningTimeFont <<timerColor <<timerFont;
    for(int i = 0;i < size; ++i){
        temp = sectionList.at(i);
        out << temp.getName() <<temp.getDuration()
            << temp.getTitle() << temp.getTitleColor() << temp.getTitleFont()
            << temp.getCentralTitle() << temp.getCentralTitleColor() << temp.getCentralTitleFont()
            << temp.getTimerColor()  << temp.getTimerFont()
            << temp.getRunningTimeColor() << temp.getRunnigTimeFont();
    }
    QApplication::restoreOverrideCursor();

    return true;
}

bool ShowWidget::readFile(const QString &fileName){
    QFile file(fileName);
    if(!file.open(QIODevice::ReadOnly)){
        QMessageBox::warning(this,tr("Timer"),tr("Cannot read file %1:\n%2.")
                             .arg(file.fileName())
                             .arg(file.errorString()));
        return false;
    }

    QDataStream in(&file);
    in.setVersion(QDataStream::Qt_4_8);

    quint32 magic;
    quint32 version;
    in >> magic;
    in >> version;
    if(magic!= MagicNumber){
        QMessageBox::warning(this,tr("Timer"),tr("This is not a Timer file."));
        return false;
    }

    QList<Section> newList;
    Section temp;
    QString name;
    QTime duration;
    QString title;
    QString centralTitle;
    QColor colTitle;
    QColor colCTitle;
    QColor color;
    QFont font;
    quint16 size;

    QApplication::setOverrideCursor(Qt::WaitCursor);

    sectionList.clear();
    in >> size;
    in >>color >>font;
    runningTimeColor = color;
    runningTimeFont = QFont(font);
    in>>color >> font;
    timerColor = color;
    timerFont = QFont(font);
    for(int i = 0; i < size;++i){
        in >> name >> duration
           >> title >>colTitle >>font;

        temp = Section(name,duration,title);
        temp.setTitleColor(colTitle);
        temp.setTitleFont(font);

        in  >>centralTitle >> colCTitle >>font;

        temp.setCentralTitle(centralTitle);
        temp.setCentralTitleColor(colCTitle);
        temp.setCentralTitleFont(font);

        in>>colTitle >>font;
        temp.setTimerColor(colTitle);
        temp.setTimerFont(font);

        in >>colTitle >>font;
        temp.setRunningTimeColor(colTitle);
        temp.setRunningTimeFont(font);

        sectionList.append(temp);
    }

    QApplication::restoreOverrideCursor();
    qDebug() <<"did read file";
    emit updateShowWidgetView();
    return true;
}

bool ShowWidget::handleNextSection(){
    int t = sectionList.size();
    qDebug() <<"handleNextsection " << t <<" " <<sectionId;
    if(sectionId <  t-1){
        sectionId++;      // move ahead
        Section curSection = sectionList.at(sectionId);
        rightText = curSection.getTitle();
        centralText = curSection.getCentralTitle();
        sectionTime = curSection.getDuration();
        timeToFinishSection = curSection.getDuration();
        qDebug() <<"handleNextsection true " << timeToFinishSection.toString();
        return true;
    }
    qDebug() <<"handleNextsection false " << timeToFinishSection.toString();
    return false;
}

//working
void ShowWidget::currentRowInViewChanged(int row){

    QTime cumulativeTime = QTime(0,0,0,0);

    for(int i = 0; i < row; i++){
        if(i < sectionList.count()){
            cumulativeTime = cumulativeTime.addSecs(
                        timeInSeconds(sectionList[i].getDuration() ));
        }else{
            cumulativeTime = cumulativeTime.addSecs(60);
        }

    }
    sectionId = row;
    presentationTime = cumulativeTime;

    if(row < sectionList.count()){
        timeToFinishSection = sectionList[row].getDuration();
    }else //adding a section
        timeToFinishSection = QTime(0,1,0,0);

    update();
    emit presentationTimeChanged(presentationTime);

    qDebug()<<"ShowWidget currentRowInViewChanged " <<row;
}

void ShowWidget::handlePresentation(){

    if(timeInSeconds(timeToFinishSection) == 0){
        if(!handleNextSection()){
            emit stop();
            return;
        }
    }
    presentationTime = presentationTime.addSecs(1);
    timeToFinishSection = timeToFinishSection.addSecs(-1);

    emit presentationTimeChanged(presentationTime);
    update();

}

void ShowWidget::increaseSpeedFactor(){
    speedFactor*=.5;
    updateTimer->setInterval(qIntCast(1000*speedFactor));
}

void ShowWidget::decreaseSpeedFactor(){
    speedFactor*=2;
    updateTimer->setInterval(qIntCast(1000*speedFactor));
}

void ShowWidget::setSpeedFactorToOne(){
    speedFactor=1;
    updateTimer->setInterval(1000);
}

void ShowWidget::sliderWasMovedbyUser(const QTime &time){
    presentationTime = time;
    emit presentationTimeChanged(time);
    //look for current section
    QTime cumulativeTime = QTime(0,0,0,0);
    Section temp;
    bool shouldContinue = true;

    for(int i=0;i<sectionList.size() && shouldContinue;i++){
        temp = sectionList.at(i);
        cumulativeTime = cumulativeTime.addSecs( timeInSeconds(temp.getDuration() ));
        if(time.operator <=(cumulativeTime)){
            sectionId = i;
            timeToFinishSection = cumulativeTime.addSecs(-timeInSeconds(time));
            shouldContinue = false;
        }
    }
    update();
}

int ShowWidget::timeInSeconds(const QTime &time){
    return QTime(0,0,0,0).secsTo(time);
}

QTime ShowWidget::totalTime(){
    Section temp;
    QTime totalQTime = QTime(0,0,0,0);
    for(int i=0;i<sectionList.size();++i){
        temp = sectionList.at(i);
        totalQTime = totalQTime.addSecs( timeInSeconds(temp.getDuration()) );
    }
    //    qDebug()<<"totalTime "<<totalQTime.toString();
    return totalQTime;
}
