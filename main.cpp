#include <QtGui/QApplication>
#include "mainwindow.h"
#include <QObject>

#ifdef Q_OS_MAC
#define RESOURCES "/../Resources"
#endif
//static const char VERSION[] = "0.1.3";

int main(int argc, char *argv[])
{
    //    if(QDateTime::currentDateTime()> QDateTime(QDate(2012,12,1))){
    //        QMessageBox msgBox;
    //        msgBox.setText(QObject::tr("Get updated at https://sites.google.com/site/kipotapp/"));
    //        msgBox.exec();
    //        return 0;
    //    }
    QApplication a(argc, argv);

    //load local translation
    QString locale = QLocale::system().name();
    QTranslator translator;
    qDebug () <<locale;
    // In windows translation file is on app folder
#ifdef Q_OS_WIN32
    translator.load(QString("timer_") + locale);
#endif
    // In Mac is in the Resources folder in the app bundle
#ifdef Q_OS_MACX
    QDir dir(QCoreApplication::applicationDirPath());
    qDebug()<<dir;
    dir.cdUp();
    translator.load(QString("timer_")+locale,dir.path()+ QString("/Resources"));
#endif
    a.installTranslator(&translator);

    MainWindow w;
    w.show();
    return a.exec();
}
