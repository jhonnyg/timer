<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="23"/>
        <location filename="mainwindow.ui" line="647"/>
        <source>Timer</source>
        <translation>Cronómetro</translation>
    </message>
    <message>
        <source>New Item 1</source>
        <translation type="obsolete">Nuevo item</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="98"/>
        <source>New Section 1</source>
        <translation>Nueva sección 1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="174"/>
        <source>Configuration</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="386"/>
        <source>HH:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="369"/>
        <source>Duration</source>
        <translation>Duración</translation>
    </message>
    <message>
        <source>At the end</source>
        <translation type="obsolete">Al final</translation>
    </message>
    <message>
        <source>Do nothing</source>
        <translation type="obsolete">No hacer nada</translation>
    </message>
    <message>
        <source>Keep counting</source>
        <translation type="obsolete">Seguir contando</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="695"/>
        <location filename="mainwindow.ui" line="827"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="701"/>
        <source>Edit</source>
        <translation>Editar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="712"/>
        <source>Tools</source>
        <translation>Herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="729"/>
        <source>View</source>
        <translation>Ver</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="735"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="obsolete">Formato</translation>
    </message>
    <message>
        <source>Font</source>
        <translation type="obsolete">Fuente</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="260"/>
        <location filename="mainwindow.ui" line="353"/>
        <location filename="mainwindow.ui" line="474"/>
        <location filename="mainwindow.ui" line="565"/>
        <source>f</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="654"/>
        <source>Central time</source>
        <translation>Tiempo central</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="674"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="678"/>
        <source>Recent Files...</source>
        <translation>Archivos recientes...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="790"/>
        <source>&amp;Open...</source>
        <translation>&amp;Abrir...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="793"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="796"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="801"/>
        <source>Quit</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="804"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="813"/>
        <source>Full Screen</source>
        <translation>Pantalla completa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="816"/>
        <source>Fullscreen</source>
        <translation>Pantalla Completa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="819"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="835"/>
        <source>ShowNormal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="838"/>
        <source>Esc</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="843"/>
        <source>&amp;New</source>
        <translation>&amp;Nuevo</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="846"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="858"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="863"/>
        <source>Save as...</source>
        <translation>Guardar como...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="868"/>
        <source>Load image...</source>
        <translation>Cargar imagen...</translation>
    </message>
    <message>
        <source>Settings...</source>
        <translation type="obsolete">Preferencias...</translation>
    </message>
    <message>
        <source>How to...</source>
        <translation type="obsolete">Cómo se hace...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="890"/>
        <source>Play</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="893"/>
        <source>Ctrl+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="964"/>
        <source>Continue at end</source>
        <translation>Continuar al final</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="973"/>
        <source>Play in fullscreen</source>
        <translation>Play en pantalla completa</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="976"/>
        <source>Ctrl+Shift+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="981"/>
        <source>Normal</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="986"/>
        <location filename="mainwindow.cpp" line="121"/>
        <source>Clear Menu</source>
        <translation>Borrar items recientes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="991"/>
        <source>New Timer</source>
        <translation>Nuevo cronómetro</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="994"/>
        <source>Ctrl+Shift+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="999"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1004"/>
        <source>Close All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1009"/>
        <source>Check for updates</source>
        <translation type="unfinished">Actualizaciones</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="898"/>
        <source>Stop</source>
        <translation>Detener</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="150"/>
        <source>00:00/01:00</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="873"/>
        <source>Settings...</source>
        <comment>Settings menu</comment>
        <translation type="unfinished">Preferencias...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="878"/>
        <source>Online help...</source>
        <translation>Ayuda online...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="903"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="908"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="911"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="916"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="919"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="924"/>
        <source>Faster</source>
        <translation>Mas rápido</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="927"/>
        <source>Alt+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="932"/>
        <source>Slower</source>
        <translation>Mas lento</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="935"/>
        <source>Alt+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="940"/>
        <source>Image controls</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="945"/>
        <source>Add Section</source>
        <translation>Añadir sección</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="948"/>
        <source>Add another section</source>
        <translation>Añadir sección</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="953"/>
        <source>Remove section</source>
        <translation>Borrar sección</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="956"/>
        <source>Remove current section</source>
        <translation>Borrar sección</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="185"/>
        <source>Open file</source>
        <translation>Abrir archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="185"/>
        <source>Kipot file *.kpt</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Save file</source>
        <translation>Guardar archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="201"/>
        <source>Kipot files (*.kpt)</source>
        <translation></translation>
    </message>
    <message>
        <source>About Timer</source>
        <translation type="obsolete">Acerca de Kipot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="222"/>
        <source>About Kipot</source>
        <translation>Sobre Kipot</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Kipot
Helps to time</source>
        <oldsource>Kipot
Helps to count</oldsource>
        <translation>Kipot
Ayuda a cronometrar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="225"/>
        <source>Version </source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="237"/>
        <source>New versions at</source>
        <translation>Nuevas versiones en</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="372"/>
        <source>New Section</source>
        <translation>Nueva sección</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="598"/>
        <source>
Hey! Do you want to save your changes?</source>
        <oldsource>Hey! Do you want to save your changes?</oldsource>
        <translation>Hey! Deseas guardar tus cambios a este archivo?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="757"/>
        <source>Untitled</source>
        <translation>Sin título</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="765"/>
        <source>%1[*] - %2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="597"/>
        <location filename="mainwindow.cpp" line="766"/>
        <source>Kipot</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="section.cpp" line="19"/>
        <source>Descriptive title</source>
        <translation>Título Descriptivo</translation>
    </message>
    <message>
        <location filename="section.cpp" line="20"/>
        <source>Central title during first section&apos;s minute, then timer</source>
        <translation>Título Central y Cronómetro</translation>
    </message>
    <message>
        <source>Get updated at https://sites.google.com/site/kipotapp/</source>
        <translation type="obsolete">Actualizaciones en https://sites.google.com/site/kipotapp/</translation>
    </message>
</context>
<context>
    <name>Section</name>
    <message>
        <source>Descriptive title</source>
        <translation type="obsolete">Título descriptivo</translation>
    </message>
    <message>
        <source>Central title during first section&apos;s minute, then timer</source>
        <translation type="obsolete">Título central, estará durante el primer minuto, luego el cronómetro</translation>
    </message>
</context>
<context>
    <name>ShowWidget</name>
    <message>
        <location filename="showwidget.ui" line="23"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="showwidget.cpp" line="32"/>
        <location filename="showwidget.cpp" line="196"/>
        <source>New Section 1</source>
        <translation>Nueva sección 1</translation>
    </message>
    <message>
        <location filename="showwidget.cpp" line="257"/>
        <location filename="showwidget.cpp" line="291"/>
        <location filename="showwidget.cpp" line="305"/>
        <source>Timer</source>
        <translation>Kipot</translation>
    </message>
    <message>
        <location filename="showwidget.cpp" line="257"/>
        <source>Cannot write file %1:
%2.</source>
        <translation>No se puede escribir archivo %1:%2.</translation>
    </message>
    <message>
        <location filename="showwidget.cpp" line="291"/>
        <source>Cannot read file %1:
%2.</source>
        <translation>No se puede leer el archivo %1:%2.</translation>
    </message>
    <message>
        <location filename="showwidget.cpp" line="305"/>
        <source>This is not a Timer file.</source>
        <translation>Este no es un archivo Kipot.</translation>
    </message>
</context>
<context>
    <name>Ui::MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="1"/>
        <source></source>
        <comment>Necessary for lupdate. ...</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>Ui_MainWindow::MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="1"/>
        <source></source>
        <comment>Necessary for lupdate. ...</comment>
        <translation></translation>
    </message>
</context>
</TS>
