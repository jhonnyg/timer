
#include "timeniceformat.h"

#include <QTime>
#include <QString>

QString timeSmartFormat(const QTime time){
    QString format;

    if(time.hour()>0)
         format = "H:mm:ss";
     else
         format = "mm:ss";

    return format;
}
