#ifndef TIMENICEFORMAT_H
#define TIMENICEFORMAT_H

class QString;
class QTime;

QString timeSmartFormat(const QTime time);

#endif // TIMENICEFORMAT_H
