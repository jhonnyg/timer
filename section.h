#ifndef SECTION_H
#define SECTION_H

#include <QWidget>
#include <QTime>
#include <QString>
//class QTime;
//what is the difference between using class QTime and including the actual library?
//class did not work if I want to return QTime objects

class Section
{

    //Q_OBJECT

public:    

    Section();
    Section(const QString &name);
    Section(const QString &name,const QTime &time, const QString &title);
    //getters and setters
    QString getName() {return name;}
    void setName(const QString &name_);

    QString getTitle() {return title;}
    void setTitle(const QString &title);

    QTime getDuration() {return *duration;}
    void setDuration(  const QTime &duration_);

    unsigned int getPosition() {return position;}
    void setPosition(unsigned int pos){ position = pos;}

    QString getCentralTitle(){return centralTitle;}
    void setCentralTitle(const QString &str);

    QColor getTitleColor(){return titleColor;}
    void setTitleColor(QColor col){titleColor=col;}

    QColor getCentralTitleColor(){return centralTitleColor;}
    void setCentralTitleColor(QColor col){centralTitleColor=col;}

    QColor getRunningTimeColor(){return runningTimeColor;}
    void setRunningTimeColor(QColor col){runningTimeColor = col;}
    QFont getRunnigTimeFont(){return runningTimeFont;}
    void setRunningTimeFont(QFont font) {runningTimeFont = QFont(font);}

    QColor getTimerColor(){return timerColor;}
    void setTimerColor(QColor col){timerColor = col;}
    QFont getTimerFont(){return timerFont;}
    void setTimerFont(QFont font) {timerFont = QFont(font);}

    QFont getTitleFont(){return titleFont;}
    void setTitleFont(QFont font){titleFont = QFont(font);}

    QFont getCentralTitleFont(){return centralTitleFont;}
    void setCentralTitleFont(QFont font){centralTitleFont = QFont(font);}


private slots:

private:

    void init();

    QString name;
    QTime *duration;
    unsigned int position;

    QString title;
    QFont titleFont;
    QColor titleColor;

    QString centralTitle;
    QFont centralTitleFont;
    QColor centralTitleColor;

    QColor runningTimeColor;
    QFont runningTimeFont;

    QColor timerColor;
    QFont timerFont;

};

#endif // SECTION_H
