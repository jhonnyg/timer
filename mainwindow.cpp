
#include <QtGui>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "timeniceformat.h"

#define KIPOT_VERSION_STR "0.1.4" // duplicate in main.cpp
#define FAQ ""

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)  //    ui(new Ui::MainWindow)
{
    setupUi(this);

    myPalette = QPalette(palette());

    plusButton = new QPushButton("+",0);
    minusButton = new QPushButton("-",0);
    plusButton->setFlat(true);
    minusButton->setFlat(true);
    Ui_MainWindow::statusBar->addWidget(plusButton,0);
    Ui_MainWindow::statusBar->addWidget(minusButton,0);
    minusButton->setEnabled(false);

    // Context Menu for listWidget
    // There is better way to do this by reimplementing the QWidget::contextMenuEvent() function.
    listWidget->addAction(actionAddSection);
    listWidget->addAction(actionRemoveSection);
    listWidget->addAction(actionDuplicate_section);
    listWidget->setContextMenuPolicy(Qt::ActionsContextMenu);

    timeEdit->setTime(QTime(0,1,0,0));

    //    showPanel->addSection(tr("New Section 1"),0);
    listWidget->setCurrentRow(0);
    showPanel->getSectionFromList(0).setDuration( QTime(0,1,0,0));

    createActions();
    //    createAnimations();
    //more menus
    for (int i = 0; i < MaxRecentFiles; ++i)
        menuRecent_Files->addAction(recentFileActions[i]);

    menuRecent_Files->addSeparator();
    menuRecent_Files->addAction(actionClear_Menu);

    readSettings();

    setCurrentFile("");
    setSliderMaxValue();
    update();
}

MainWindow::~MainWindow()
{

}

// add connections of existing actions and create some other actions
void MainWindow::createActions()
{
    //menu actions
    connect(actionOpen, SIGNAL(triggered()),this,SLOT(open()));
    connect(actionSave,SIGNAL(triggered()),this,SLOT(save()));
    connect(actionNew,SIGNAL(triggered()),this,SLOT(newFile()));
    connect(actionNewTimer,SIGNAL(triggered()),this,SLOT(newTimer()));

    connect(actionSave_as,SIGNAL(triggered()),this,SLOT(saveAs()));
    connect(actionFullscreen,SIGNAL(triggered()),this,SLOT(fullscreen()));

    //work around when app is in full screen, if menu is invisible actions shall be disabled
    // in Mac OSX the menu is onlyput behind the app and will appear when mouse is over it
#ifdef Q_OS_WIN32
    showPanel->addAction(actionFullscreen);
    showPanel->addAction(actionPlay);
#endif

    connect(actionShowNormal,SIGNAL(triggered()),this,SLOT(showNormal()));
    connect(actionPlay,SIGNAL(toggled(bool)),this,SLOT(play(bool)));
    connect(actionStop,SIGNAL(triggered()),this,SLOT(stop()));
    connect(actionPlay_in_fullscreen,SIGNAL(triggered()),this,SLOT(play()));
    connect(actionPlay_in_fullscreen,SIGNAL(triggered()),this,SLOT(fullscreen()));
    connect(actionHow_to,SIGNAL(triggered()),this,SLOT(help()));
    connect(actionAbout,SIGNAL(triggered()),this,SLOT(about()));
    connect(actionCheck_for_updates,SIGNAL(triggered()),this,SLOT(checkNewVersion()));
    connect(showPanel,SIGNAL(stop()),actionPlay,SLOT(toggle()));

    //For the QListWidget
    connect(plusButton,SIGNAL(clicked()),this,SLOT(addSection()));
    connect(minusButton,SIGNAL(clicked()),this,SLOT(removeSection()));
    connect(actionAddSection,SIGNAL(triggered()),this,SLOT(addSection()));
    connect(actionRemoveSection,SIGNAL(triggered()),this,SLOT(removeSection()));
    connect(actionDuplicate_section,SIGNAL(triggered()),this,SLOT(duplicateSection()));

    actionCopy_Section = new QAction(tr("Copy Section"),this);
    actionCopy_Section->setShortcutContext(Qt::WindowShortcut);
    actionCopy_Section->setShortcut(QKeySequence::Copy);
    listWidget->addAction(actionCopy_Section);
    connect(actionCopy_Section,SIGNAL(triggered()),this,SLOT(copySection()));

    //    connect(listWidget,SIGNAL())
    connect(listWidget,SIGNAL(drop(int,int)),this,SLOT(drop(int,int)));

    connect(listWidget,SIGNAL(currentRowChanged(int)),this,SLOT(currentItemChanged(int)));
    connect(listWidget,SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)),
            this,SLOT(currentItemChanged(QListWidgetItem*,QListWidgetItem*)));
    //tell showPanel current item changed
    //    connect(listWidget,SIGNAL(currentRowChanged(int)),showPanel,SLOT(currentRowInViewChanged(int)));
    connect(listWidget,SIGNAL(itemChanged(QListWidgetItem*)),this,SLOT(sectionNameChanged()));

    //    connect(listWidget,SIGNAL(currentRowChanged(int)),this,SLOT(setSliderMaxValue()));
    //test signals for qlistwidget
    connect(listWidget,SIGNAL(itemChanged(QListWidgetItem*)),this,SLOT(test()));
    connect(timeEdit,SIGNAL(timeChanged(QTime)),this,SLOT(timeChanged(QTime)));
    //    connect(timeEdit,SIGNAL(timeChanged(QTime)),this,SLOT(setSliderMaxValue()));

    connect(actionFaster,SIGNAL(triggered()),showPanel,SLOT(increaseSpeedFactor()));
    connect(actionSlower,SIGNAL(triggered()),showPanel,SLOT(decreaseSpeedFactor()));
    connect(actionNormalSpeed,SIGNAL(triggered()),showPanel,SLOT(setSpeedFactorToOne()));

    connect(showSlider,SIGNAL(sliderMoved(int)),this,SLOT(sliderMovedbyUser(int)));
    connect(showPanel,SIGNAL(presentationTimeChanged(QTime)),this,SLOT(updateSlider(QTime)));
    connect(showPanel,SIGNAL(presentationTimeChanged(QTime)),this,SLOT(updateLabel(QTime)));
    connect(showPanel,SIGNAL(updateShowWidgetView()),this,SLOT(updateView()));

    //Recent Files
    for (int i = 0; i < MaxRecentFiles; ++i) {
        recentFileActions[i] = new QAction(this);
        recentFileActions[i]->setVisible(false);
        connect(recentFileActions[i], SIGNAL(triggered()),
                this, SLOT(openRecentFile()));
    }

    actionClear_Menu = new QAction(this);
    actionClear_Menu->setObjectName(QString::fromUtf8("actionClear_Menu"));
    actionClear_Menu->setText(QApplication::translate("MainWindow", "Clear Menu", 0, QApplication::UnicodeUTF8));
    connect(actionClear_Menu,SIGNAL(triggered()),this,SLOT(clearMenu()));

    //---------- Configuration Panel
    connect(titleLineEdit,SIGNAL(editingFinished()),this,SLOT(titleChanged()));
    connect(pushButtonTitleColor,SIGNAL(clicked()),this,SLOT(titleColorChanged()));
    connect(pushButtonTitleFont,SIGNAL(clicked()),this,SLOT(titleFontChanged()));

    connect(centralTitleLineEdit,SIGNAL(editingFinished()),this,SLOT(centralTitleChanged()));
    connect(pushButtonCentralTitleColor,SIGNAL(clicked()),this,SLOT(centralTitleColorChanged()));
    connect(pushButtonCentralTitleFont,SIGNAL(clicked()),this,SLOT(centralTitleFontChanged()));

    connect(pushButtonRunColor,SIGNAL(clicked()),this,SLOT(runningTimeColorChanged()));
    connect(pushButtonRunFont,SIGNAL(clicked()),this,SLOT(runningFontChanged()));
    connect(pushButtonTimerColor,SIGNAL(clicked()),this,SLOT(timerColorChanged()));
    connect(pushButtonTimerColor_2,SIGNAL(clicked()),this,SLOT(timerColor2Changed()));
    connect(pushButtonTimerFont,SIGNAL(clicked()),this,SLOT(timerFontChanged()));

    connect(qApp,SIGNAL(focusChanged(QWidget*,QWidget*)),this,SLOT(updateWidgetFocus(QWidget*,QWidget*)));

    actionSettings->setVisible(false);
    actionLoad_image->setVisible(false);
    actionImage_controls->setVisible(false);
    actionNewTimer->setVisible(false);
    actionClose->setVisible(false);
    actionClose_All->setVisible(false);


}

void MainWindow::updateWidgetFocus(QWidget *old, QWidget *neww){
    oldWidget= old;
    newWidget = neww;
    if(old!=0 && neww!=0)
        qDebug() <<"focus changed "<<old->objectName()
                << " " <<neww->objectName();
}

// new file with one section
void MainWindow::newFile(){
    if(okToContinue()){
        //clear listwidget
        showPanel->removeAll();
        updateView();
        setCurrentFile("");
        listWidget->setCurrentRow(0);
    }
}

void MainWindow::newTimer(){
    MainWindow *newTimer = new MainWindow(0);
    newTimer->show();
}

void MainWindow::help(){
    QDesktopServices::openUrl(QUrl("https://sites.google.com/site/kipotapp/faq", QUrl::TolerantMode));
}

void MainWindow::open()
{
    if(okToContinue()){
        QString filename = QFileDialog::getOpenFileName(this,
                                                        tr("Open file"),".",tr("Kipot file *.kpt"));
        if(!filename.isEmpty()){
            loadFile(filename);
        }
    }
}

bool MainWindow::save(){
    if(currentFile.isEmpty()){
        return saveAs();
    }else{
        return saveFile(currentFile);
    }
}

bool MainWindow::saveAs(){
    QString fileName = QFileDialog::getSaveFileName(this,tr("Save file"),".",tr("Kipot files (*.kpt)"));
    if (fileName.isEmpty())
        return false;

    return saveFile(fileName);
}

void MainWindow::showWidgetmodified(){

}

void MainWindow::openRecentFile(){
    if (okToContinue()) {
        QAction *action = qobject_cast<QAction *>(sender());
        if (action)
            loadFile(action->data().toString());
    }
}

void MainWindow::about(){
    QUrl  url;
    url.setUrl("https://sites.google.com/site/kipotapp");
    QMessageBox::about(this,tr("About Kipot"),
                       QString("%1\n%2 %3 \n\n%4 \n\n%5 ").
                       arg(tr("Kipot")).
                       arg(tr("Version ")).
                       arg(KIPOT_VERSION_STR).
                       arg("Copyright 2011 - 2012 Jhonny Gonzalez").
                       arg(QString(url.toEncoded() )));

}

void MainWindow::checkNewVersion(){
    //see on Qmessagebox documentation constructor
    //n Mac OS X, if you want your message box to appear as a
    //Qt::Sheet of its parent, set the message box's window modality to Qt::WindowModal
    //or use open(). Otherwise, the message box will be a standard dialog.
    QMessageBox versionBox(this);
    versionBox.setText(QString("Kipot %1\n\n%2\n%3").arg(KIPOT_VERSION_STR).arg(tr("New versions at")).arg("https://sites.google.com/site/kipotapp"));
    versionBox.exec();
}

void MainWindow::fullscreen()
{
    bool f = isFullScreen();
    if(true){
        mainToolBar->setVisible(f);
        plusButton->setVisible(f);
        minusButton->setVisible(f);
        listWidget->setVisible(f);
        groupBox->setVisible(f);
        timeLabel->setVisible(f);
        menuWidget()->setVisible(f);
        showSlider->setVisible(f);
        actionPlay_in_fullscreen->setDisabled(!f);
        Ui_MainWindow::statusBar->setVisible(f);
    }

    if(!f){
        //         animation->setDuration(500);
        //         animation->setStartValue(geometry());
        //         animation->setEndValue(QRect(0, 0, qApp->desktop()->width(),qApp->desktop()->height()));
        //         animation->start();
        hide();
        Ui_MainWindow::centralWidget->layout()->setContentsMargins(0,0,0,0);
        showFullScreen();
        QApplication::setOverrideCursor(Qt::BlankCursor);

        //        Ui_MainWindow::gridLayout->setSpacing(0);

        qDebug() <<Ui_MainWindow::centralWidget->layout()->contentsMargins();
        qDebug()<<gridLayout_2->layout()->contentsMargins() ;

        setPalette(Qt::black);
        //save window state QWidget::setWindowState
        actionFullscreen->setShortcut(Qt::Key_Escape);
    }else{
        QApplication::setOverrideCursor(Qt::ArrowCursor);
        hide();
        Ui_MainWindow::centralWidget->layout()->setContentsMargins(12,12,12,12);
        setPalette(myPalette);
        showNormal();
        //        setWindowState(windowState() & ~Qt::WindowFullScreen);
        show();
        actionFullscreen->setShortcut(Qt::CTRL+Qt::Key_F);
    }
}

void MainWindow::addSection(){
    QListWidgetItem *newItem = new QListWidgetItem;
    newItem->setText(tr("New Section"));
    newItem->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable
                      |Qt::ItemIsEditable|Qt::ItemIsDragEnabled);
    newItem->setIcon(QIcon(":images/icons/section2.png"));
    int row = listWidget->currentRow();
    //it should exist first in showPanel
    showPanel->addSection(newItem->text(),row + 1);
    listWidget->insertItem(row + 1, newItem);
    listWidget->editItem(newItem);
    listWidget->setCurrentRow(row + 1);

    if(listWidget->count()!=1)
        minusButton->setEnabled(true);

    setSliderMaxValue();
    qDebug() <<"MainWindow addSection";
    setWindowModified(true);
}

void MainWindow::removeSection(){
    int row = listWidget->currentRow();
    int count = listWidget->count();
    if(count!=1){
        delete listWidget->takeItem(row);
        if(row==0)
            listWidget->setCurrentRow(0);
        else
            listWidget->setCurrentRow(row-1);

        showPanel->removeSection(row);
    }

    if(listWidget->count()==1)
        minusButton->setEnabled(false);

    //    Ui_MainWindow::statusBar->showMessage(QString("").setNum(count),2000);

    setSliderMaxValue();
    qDebug() << "removing section";
    setWindowModified(true);
}

void MainWindow::duplicateSection(){
    int row = listWidget->currentRow();
    showPanel->duplicateSection(row);
    listWidget->setCurrentRow(row+1);
}

void MainWindow::copySection()
{
    qDebug() <<listWidget->actions();
    qDebug() <<"Copying section";
}

void MainWindow::drop(int from, int to)
{
//    qDebug()<< " MainWindow::drop "<<row; // new position
    int t = listWidget->currentRow(); // selected row
    showPanel->drop(from, to);
//    listWidget->setDropping(false);
}

void MainWindow::play(bool playIsChecked){        
    showPanel->play(playIsChecked);
    actionPlay->setChecked(playIsChecked);
    //    showPanel->setSectionId(listWidget->currentRow());
    //create a method for this below
    //more like widget containg all this setEnabled(fase) // should put gui in != widget??
    //    listWidget->setEnabled(!playIsChecked);
    //    plusButton->setEnabled(!playIsChecked);
    //    minusButton->setEnabled(!playIsChecked);
    //    plusButton->setEnabled(!playIsChecked);

}

void MainWindow::play(){
    if(!actionPlay->isChecked())
        play(true);
}

void MainWindow::stop(){
    play(false);
}

//this function is called always before addSection, if the plusButton is pressed
void MainWindow::currentItemChanged(int row){
    qDebug()<<"currentItemChanged main "<<row;
    //    int row = listWidget->currentRow();
    //verify row is in range
    if (row<0)
        row = 0;
    int tt = showPanel->countSections()-1;
    if (row<=tt && row>-1)
        showPanel->currentRowInViewChanged(row);
    if(isAddingSection())
        setWindowModified(true);
//    if(listWidget->isDropping())
    {
//        qDebug()<<"MWindow #############################is Droppping";
//        this->drop(listWidget->getInit(),row);
    }
    update();
    qDebug() <<"MainWindow currentItemChanged() y";

}

void MainWindow::currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
//    if(listWidget->getisDropping())

//    qDebug() <<"MainWindow currentItemChanged current "<<listWidget->row(current);
//    qDebug() <<"MainWindow currentItemChanged previous "<<listWidget->row(previous);
}
void MainWindow::update(){

    Section temp = showPanel->getSectionFromList(listWidget->currentRow());
    timeEdit->setTime(temp.getDuration());
    titleLineEdit->setText(temp.getTitle());
    centralTitleLineEdit->setText(temp.getCentralTitle());
    pushButtonTitleColor->setPalette(QPalette(temp.getTitleColor()));
    pushButtonCentralTitleColor->setPalette(QPalette(temp.getCentralTitleColor()));
    pushButtonTimerColor->setPalette(QPalette(showPanel->getTimerColor()));
    pushButtonTimerColor_2->setPalette(QPalette(showPanel->getTimer2Color()));
    showPanel->update();
}

void MainWindow::sectionNameChanged(){
    int row = listWidget->currentRow();
    showPanel->textChanged(listWidget->item(row)->text(),row);

    qDebug() <<"MainWindow textChanged()";
    setWindowModified(true);
}

void MainWindow::timeChanged(const QTime &time){
    int row = listWidget->currentRow();
    int count = showPanel->countSections();

    if(isAddingSection()){//adding a section
        //        timeEdit->setTime(QTime(0,1,0,0));
    }else{  //existing section
        //carefull mainwindow is making changes in showPanel
        // could just send information
        showPanel->getSectionFromList(row).setDuration(time);
    }

    setSliderMaxValue();

    qDebug() <<"MainWindow timeChanged";
    setWindowModified(true);
    updateLabel(QTime(0,0,0,0).addSecs(showSlider->value()));
}

void MainWindow::titleChanged(){
    showPanel->setSectionTitle(listWidget->currentRow(),titleLineEdit->text());
    setWindowModified(true);
}

void MainWindow::centralTitleChanged(){
    showPanel->setSectionCentralTitle(listWidget->currentRow(),centralTitleLineEdit->text());
    setWindowModified(true);
}

void MainWindow::centralTitleFontChanged(){
    bool ok = false;
    int row = listWidget->currentRow();
    QFont font = QFontDialog::getFont(&ok,showPanel->getSectionFromList(row).getCentralTitleFont(), this);
    if(ok){
        showPanel->getSectionInList(row)->setCentralTitleFont(font);
        showPanel->update();
        setWindowModified(true);
    }
}

void MainWindow::titleColorChanged(){
    int row = listWidget->currentRow();
    QColor color = QColorDialog::getColor(showPanel->getSectionFromList(row).getTitleColor(),this);
    if(!color.isValid())
        return;
    showPanel->setSectionTitleColor(listWidget->currentRow(),color);
    pushButtonTitleColor->setPalette(QPalette(color));
    setWindowModified(true);

}

void MainWindow::titleFontChanged(){
    bool ok = false;
    int row = listWidget->currentRow();
    QFont font = QFontDialog::getFont(&ok,showPanel->getSectionFromList(row).getTitleFont(), this);
    if(ok){
        showPanel->getSectionInList(row)->setTitleFont(font);
        showPanel->update();
        setWindowModified(true);
    }
}

void MainWindow::centralTitleColorChanged(){
    int row = listWidget->currentRow();
    QColor color = QColorDialog::getColor(showPanel->getSectionFromList(row).getCentralTitleColor(),this);
    if(!color.isValid())
        return;
    showPanel->setSectionCentralTitleColor(row,color);
    pushButtonCentralTitleColor->setPalette(QPalette(color));
    setWindowModified(true);
}

void MainWindow::runningTimeColorChanged(){
//    int row = listWidget->currentRow();
    QColor color = QColorDialog::getColor(showPanel->getRunninTimeColor(),this);
    if(!color.isValid())
        return;
    showPanel->setRunningTimeColor(color);
    showPanel->update();
    pushButtonRunColor->setPalette(QPalette(color));
    qDebug() <<color;
    setWindowModified(true);
}

void MainWindow::runningFontChanged(){
    bool ok = false;
    int row = listWidget->currentRow();
    QFont font = QFontDialog::getFont(&ok,showPanel->getRunningTimeFont(), this);
    if(ok){
        showPanel->setRunningTimeFont(font);
        showPanel->update();
        setWindowModified(true);
    }
}

void MainWindow::timerColorChanged(){
    int row = listWidget->currentRow();
    QColor color = QColorDialog::getColor(showPanel->getTimerColor(),this);
    if(!color.isValid())
        return;
    showPanel->setTimerColor(color);
    showPanel->update();
    pushButtonTimerColor->setPalette(QPalette(color));
    setWindowModified(true);
}
void MainWindow::timerColor2Changed(){
    int row = listWidget->currentRow();
    QColor color = QColorDialog::getColor(showPanel->getTimer2Color(),this);
    if(!color.isValid())
        return;
    showPanel->setTimer2Color(color);
    showPanel->update();
    pushButtonTimerColor_2->setPalette(QPalette(color));
    setWindowModified(true);
}

void MainWindow::timerFontChanged(){
    bool ok = false;
    int row = listWidget->currentRow();
    QFont font = QFontDialog::getFont(&ok,showPanel->getTimerFont(), this);
    if(ok){
        showPanel->setTimerFont(font);
        showPanel->update();
        setWindowModified(true);
    }
}

bool MainWindow::okToContinue(){
    if (isWindowModified()) {
        int r = QMessageBox::warning(this, tr("Kipot"),
                                     tr("\nHey! Do you want to save your changes?"),
                                     QMessageBox::Yes | QMessageBox::No
                                     | QMessageBox::Cancel);
        if (r == QMessageBox::Yes) {
            return save();
        } else if (r == QMessageBox::Cancel) {
            return false;
        }
    }

    return true;
}

void MainWindow::test(){
    qDebug() <<"test";

    //    currentItemChanged() & currentRowChanged
    //    if selected item changes, when adding/erasing a section,

    //    itemChanged, only if the selected item changes, like changing name...
}

bool MainWindow::isAddingSection(){
    int row = listWidget->currentRow();
    int count = showPanel->countSections();
    if (count!=listWidget->count()){
        qDebug() <<"Adding section";
        return true;
    }
    else
            return false;


    if(count-1 < row){//adding a section
        qDebug() <<"Adding section";
        return true;
    }
    else //section exists
        return false;

    qDebug() <<"MainWindow isAddingsection()";

}

void MainWindow::closeEvent(QCloseEvent *event){
    if(okToContinue()){
        writeSettings();
        event->accept();
    }else{
        event->ignore();
    }
}

void MainWindow::writeSettings(){
    QSettings settings("Jag Inc.", "Timer pro");

    settings.setValue("geometry", saveGeometry());
    settings.setValue("state",saveState());
    settings.setValue("recentFiles", recentFiles);
}

void MainWindow::readSettings(){
    QSettings settings("Jag Inc.", "Timer pro");

    //    restoreGeometry(settings.value("geometry").toByteArray());

    //    restoreState(settings.value("state").toByteArray());

    recentFiles = settings.value("recentFiles").toStringList();
    updateRecentFileActions();

}

bool MainWindow::loadFile(const QString &fileName){
    if(!showPanel->readFile(fileName)){
        return false;
    }
    setCurrentFile(fileName);
    listWidget->setCurrentRow(0);
    return true;
}

bool MainWindow::saveFile(const QString &fileName){
    if(!showPanel->writeFile(fileName))
        return false;

    setCurrentFile(fileName);
    return true;
}

QString MainWindow::strippedName(const QString &fullFileName){
    return QFileInfo(fullFileName).fileName();
}

void MainWindow::selectFont(){
    bool ok;
    QFont font = QFontDialog::getFont(&ok,this);
    if(ok){

    }
}

void MainWindow::selectColor(){
    //    if(titleLineEdit->hasFocus())
    QFont serif("times",12);
    serif.setBold(true);
    titleLineEdit->setFont(serif);
}

void MainWindow::configChanged(){

}

void MainWindow::sliderMovedbyUser(int value){

    showPanel->sliderWasMovedbyUser(QTime(0,0,0,0).addSecs(value));

    //    qDebug() <<"sliderMovedbyUser";
}

void MainWindow::updateLabel(QTime time){
    timeLabel->setText(QString("%1/%2").arg(time.toString(timeSmartFormat(time))).
                       arg(showPanel->totalTime().toString(timeSmartFormat(showPanel->totalTime()))));
}

void MainWindow::updateSlider(QTime time){
    int secs = -time.secsTo(QTime(0,0,0,0));
    showSlider->setValue(secs);

    updateLabel(time);
    //    qDebug() << "updating slider";
}

// used when opening a new file
void MainWindow::updateView(){

    int size = showPanel->countSections();
    int count= listWidget->count();

    qDebug() << "MainWindow update view size " << size << count;

    for(int i =0 ; i<size; i++){
        QListWidgetItem *temp = new QListWidgetItem;
        temp->setFlags(Qt::ItemIsEnabled|Qt::ItemIsSelectable
                       |Qt::ItemIsEditable|Qt::ItemIsDragEnabled);
        temp->setIcon(QIcon(":images/icons/section2.png"));
        temp->setText(showPanel->getSectionFromList(i).getName());
        listWidget->insertItem(i,temp);
    }
    count= listWidget->count();
    int count2;
    for(int i=count;i>=size;i--){
        delete listWidget->takeItem(i);
        count2= listWidget->count();
    }

    if(listWidget->count()!=1)
        minusButton->setEnabled(true);
}

void MainWindow::setSliderMaxValue(){
    showSlider->setMaximum(QTime(0,0,0,0).secsTo(showPanel->totalTime()));
    qDebug()<<showSlider->maximum();
}

void MainWindow::setCurrentFile(const QString &fileName){
    currentFile = fileName;
    setWindowModified(false);

    QString nameToShow = tr("Untitled");
    if (!currentFile.isEmpty()) {
        nameToShow = strippedName(currentFile);
        recentFiles.removeAll(currentFile);
        recentFiles.prepend(currentFile);
        updateRecentFileActions();
    }

    setWindowTitle(tr("%1[*] - %2").arg(nameToShow)
                   .arg(tr("Kipot")));
}

void MainWindow::updateRecentFileActions(){
    QMutableStringListIterator i(recentFiles);

    //remove files which exist no more
    while(i.hasNext()){
        if(!QFile::exists(i.next()))
            i.remove();
    }

    //we have now create (invisible) actions, put them in the menu
    //this method was called after reading Settings in that method
    //Now we need to show existing recent Files
    for(int j=0;j<MaxRecentFiles;++j){
        if(j < recentFiles.count()){
            recentFileActions[j]->setText(recentFiles[j]);
            recentFileActions[j]->setData(recentFiles[j]);
            recentFileActions[j]->setVisible(true);
        }else{
            recentFileActions[j]->setVisible(false);
        }
    }

}

void MainWindow::clearMenu(){
    recentFiles.clear();
    updateRecentFileActions();

}

